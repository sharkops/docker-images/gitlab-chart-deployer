#!/bin/bash

# Dependencies - jq, helm 3.x, base64, wget, curl

#CI_PROJECT_ID=21907626
#CI_PROJECT_NAMESPACE=sharkops
#CI_PROJECT_NAME=cluster-tools

PAGES_URL_INTERNAL=${PAGES_URL:-$CI_PAGES_URL}

# Get Releases for your project, relies on only have a single "link" per release
RELEASES=$(curl -0 ${CI_API_V4_URL}/projects/$CI_PROJECT_ID/releases)
EXISTING_INDEX_URL=${PAGES_URL_INTERNAL}/index.yaml

# Create public directory
mkdir -p ./public

# Create robots.txt
printf "User-Agent: *\nDisallow: /" > ./public/robots.txt

# Retrieve the current index.yaml (if it exists)
EXISTING_INDEX_EXISTS=$(curl -o /dev/null -i -w "%{http_code}" $EXISTING_INDEX_URL)

echo "Generating index.yaml"
if [ $EXISTING_INDEX_EXISTS -eq "200" ]; then
  echo "Using existing index.yaml"
  wget $EXISTING_INDEX_URL
  helm repo index --merge index.yaml --url ${PAGES_URL_INTERNAL} .
else
  echo "No exsiting index.yaml"
  helm repo index --url ${PAGES_URL_INTERNAL} .
fi

cp index.yaml ./public/


echo "Downloading Releases"
for row in $(echo "${RELEASES}" | jq -r '.[] | @base64'); do
    _jq() {
     echo ${row} | base64 --decode | jq -r ${1}
    }

   package=$(_jq '.assets.links[0].direct_asset_url')
   filename=$(_jq '.assets.links[0].name')

   echo Package: ${package}
   curl --header "JOB-TOKEN: $CI_JOB_TOKEN" ${package} -o ./public/${filename}
   printf "\r\nDownloaded\r\n"
done


cp *.tgz ./public/

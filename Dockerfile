FROM registry.gitlab.com/sharkops/docker-images/k8s-cli-tools:1.0.1

RUN apk add --no-cache --update jq coreutils \
  && rm -rf /var/cache/apk/*

COPY deploy.sh .

LABEL maintaner="dewdew@gmail.com Andrew Landsverk"

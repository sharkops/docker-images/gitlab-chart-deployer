# gitlab-chart-deployer

This image can be used to host a Helm chart repository using GitLab Pages.

## Installed CLI tools

- `kubectl`
- `helm`
- `rancher`
- `bash`
- `curl`
- `wget`
- `ca-certificates`
- `git`
- `jq`
- `base64`

## Using the Image

This image is mostly intended to be used on GitLab CI in order to host a Helm repo on GitLab Pages. You can find the latest version in the [GitLab Container Registry](https://gitlab.com/sharkops/docker-images/gitlab-chart-deployer/container_registry) or in the [Releases Section](https://gitlab.com/sharkops/docker-images/gitlab-chart-deployer/-/releases)

### Locally, Using docker

```bash
docker pull registry.gitlab.com/sharkops/docker-images/gitlab-chart-deployer:latest
```

### In a Stage on GitLab CI/CD

```yaml
stages:
  - build
  - upload
  - release
  - deploy

variables:
  PACKAGE_NAME: "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}.tgz"
  PACKAGE_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}"

build:
  image: quantas/k8s-cli-tools:latest
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - helm dependency update
    - helm package --version $CI_COMMIT_REF_NAME .
  artifacts:
    paths:
        - "*.tgz"

upload:
  image: curlimages/curl:latest
  stage: upload
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${PACKAGE_NAME}" ${PACKAGE_URL}/${PACKAGE_NAME}

release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" \
        --assets-link "{\"name\":\"${PACKAGE_NAME}\",\"url\":\"${PACKAGE_URL}/${PACKAGE_NAME}\"}"

pages:
  stage: deploy
  image: registry.gitlab.com/sharkops/docker-images/gitlab-chart-deployer:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - bash deploy.sh
  artifacts:
    paths:
      - public
```
